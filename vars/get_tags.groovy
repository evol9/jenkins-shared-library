#!/usr/bin/env groovy


def call(branch){
    registry = "cyber1.project.lan/mcapi"
    branch_tag = "${registry}:${branch.full_name}"
    tags = [
        "master": [branch_tag],
        "development": [branch_tag, "${registry}:latest"],
        "demo": [branch_tag, "${registry}:demo"]
    ][branch.name]
    return tags
}
