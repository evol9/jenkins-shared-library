#!/usr/bin/env groovy


def call(action, branch, tags){

    if (action == 'build'){
        build_args = "--build-arg git_user=${GIT_CREDS_USR} --build-arg git_password=${GIT_CREDS_PSW} --network=host"
        tags = tags.collect{"-t " + it}.join(" ")
        return "docker build . ${tags} ${build_args}"
    }
    return tags.collect{"docker push " + it}.join(" && ")
}
