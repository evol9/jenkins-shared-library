#!/usr/bin/env groovy
import groovy.json.JsonOutput


def call(){
    def git_commit_hash = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
    def tags = []
    def status = sh(returnStatus: true, script: 'git describe --abbrev=0 --tags')
    if (status == 0){
		def git_tags = sh(returnStdout: true, script: 'git describe --abbrev=0 --tags').trim()
    	for( String value : git_tags.split())
        	tags.add(value);
    }
    
    def data = ["hash": git_commit_hash, "tags": tags]
    def json = JsonOutput.toJson(data)
    json = JsonOutput.prettyPrint(json)
    writeFile(file:'version.json', text: json)
}
