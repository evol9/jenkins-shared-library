#!/usr/bin/env groovy


def call(){
    if (GIT_BRANCH in ["master", "development"]){
        return ["name": GIT_BRANCH, "full_name": GIT_BRANCH]
    }
    if (GIT_BRANCH.startsWith("demo-")){
        return ["name": "demo", "full_name": GIT_BRANCH]
    }
    return []
}
